package businessLayer;
import java.io.Serializable;
import java.util.*;
public class CompositeProduct extends MenuItem implements Serializable {
	private static final long serialVersionUID = 1L;
	private ArrayList<MenuItem> items;
	
	public CompositeProduct(String name ,ArrayList<MenuItem> list) { 
		super(name, 0);  
		int price = 0;
		for(MenuItem i : list) 
			price+=i.price;  
		this.setPrice(price);
		this.items = list;
	} 
	public ArrayList<MenuItem> getIngredients(){
		return this.items;
	}
	@Override
	public int computePrice() {
		int price = 0; 
		for(MenuItem i : this.items) 
			price+=i.price; 
		this.setPrice(price);
		return price;
	}
	
}
