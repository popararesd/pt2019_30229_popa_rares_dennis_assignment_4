package businessLayer;

import java.io.Serializable;
import java.util.Date;

public class Order implements Serializable { 
	private static final long serialVersionUID = 1L;
	private int id; 
	private Date date; 
	private int tableId; 
	
	public Order(int id , int tableId) {
		super(); 
		this.id = id; 
		this.date = new Date(); 
		this.tableId = tableId;
	}   
	
	
	
	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public Date getDate() {
		return date;
	}



	public void setDate(Date date) {
		this.date = date;
	}



	public int getTableId() {
		return tableId;
	}



	public void setTableId(int tableId) {
		this.tableId = tableId;
	}



	public String toString() {
		return "Order with id="+this.id+" for the table with id="+this.tableId;
	}
	
	@SuppressWarnings("deprecation")
	public int hashCode() {
		return this.id * (this.date.getSeconds()/(this.tableId+1)) + this.date.getHours();
	}
}
