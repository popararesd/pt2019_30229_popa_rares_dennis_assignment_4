package businessLayer;

import java.util.ArrayList;
import java.util.Map;

import javax.swing.*;
public interface IRestaurantProcessing { 
	// MENU OPTIONS  
	public Map<Order,ArrayList<MenuItem>> getMap();
	
	public void removeItemFromMenu(int i); 
	public void removeItemFromMenu(MenuItem i);  
	public void removeItemFromMenu(String name);
	public void addItemInMenu(MenuItem i);  
	public void editMenuItem(String CurrentName , String NewName , int price); 
	public JTable getTabelMenu();
	//ORDER OPTIONS
	public void processByOrder(int orderId); 
	public void processByTable(int tableID); 
	public void placeOrder(int orderId,int tableId,ArrayList<MenuItem> list);
	public int getTotalPrice(int OrderId); 
	public ArrayList<MenuItem> getMenu(); 
	public JTable getTableOrders();
	
}
