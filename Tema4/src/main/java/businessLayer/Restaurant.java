package businessLayer;

import java.io.Serializable;
import java.util.*;

import javax.swing.JTable;

import dataLayer.RestaurantSerializor;

@SuppressWarnings("deprecation")
public class Restaurant extends Observable implements Serializable, IRestaurantProcessing { 
	
	private static final long serialVersionUID = 1L;
	private Map<Order,ArrayList<MenuItem>> map; 
	private ArrayList<MenuItem> menu; 
	
	public Restaurant () { 
		this.map = null; 
		this.menu = null;
	} 
	
	public void mockInitialise() {
		this.map = new HashMap<Order, ArrayList<MenuItem>>(); 
		this.menu = new ArrayList<MenuItem>();
	}

	public void setMap(Map<Order, ArrayList<MenuItem>> map) {
		this.map = map;
	}

	public void setMenu(ArrayList<MenuItem> menu) {
		this.menu = menu;
	}   
	
	
	
	public Map<Order, ArrayList<MenuItem>> getMap() {
		return map;
	}

	public ArrayList<MenuItem> getMenu() {
		return menu;
	}  
	
	public void editMenuItem(String CurrentName, String NewName, int price) {
		for(MenuItem i : this.menu) {
			if(i.getName().equalsIgnoreCase(CurrentName)) {
				i.setName(NewName); 
				i.setPrice(price);  
				break;
			}
		} 
		this.updateRestaurant();
	} 
	
	public int getTotalPrice(int OrderId) { 
		int total = 0;
		for(Map.Entry<Order, ArrayList<MenuItem>> entry : this.map.entrySet()) {
			Order order = entry.getKey(); 
			if(order.getId() == OrderId) {
				for(MenuItem i : entry.getValue()) 
					total += i.computePrice(); 
				break;
			}
		} 
		return total;
	} 
	/**
	 * 
	 * @param !menu.isEmpty() && name != null 
	 * @pre menu.size() > 0
	 * @post menu.size() == menu@pre.size() - 1
	 */
	public void removeItemFromMenu(String name) { 
		assert(this.menu.size() > 0); 
		int preSize = this.menu.size();
		for(MenuItem i : this.menu) {
			if(i.getName().equalsIgnoreCase(name)) {
				this.menu.remove(i); 
				break;
			}
		} 
		this.updateRestaurant(); 
		assert(preSize == this.menu.size() + 1);
	}
	
	
	/**
	 * 
	 * @param i 
	 * @pre (0<=i) && (i<menu.size()) 
	 * @post menu.size() == menu@pre.size() - 1;
	 */
	public void removeItemFromMenu(int i) { 
		assert(0<=i && i<menu.size()); 
		int preSize = this.menu.size();
		this.menu.remove(i); 
		this.updateRestaurant(); 
		assert(preSize == this.menu.size()+1);
	}
	/**
	 * 
	 * @param i 
	 * @pre menu.contains(i) 
	 * @post menu.size() == menu@pre.size() - 1;
	 */
	public void removeItemFromMenu(MenuItem i) { 
		assert(menu.contains(i)); 
		int preSize = menu.size();
		this.menu.remove(i); 
		this.updateRestaurant();  
		assert(preSize == this.menu.size()+1);
	}
	/**
	 * 
	 * @param !menu.contains(i) 
	 * @pre !menu.contains(i);
	 * @post menu.size() == menu@pre.size() + 1
	 */
	public void addItemInMenu(MenuItem i) { 
		assert(!menu.contains(i)); 
		int preSize = this.menu.size();
		this.menu.add(i); 
		this.updateRestaurant();  
		assert(preSize == this.menu.size()-1);
	} 
	
	/**
	 * 
	 * @param !map.isEmpty() 
	 * @pre map.size() > 0
	 * @post map.size() == map@pre.size() - 1
	 */
	public void processByOrder(int orderId) { 
		assert(this.map.size()>0); 
		int preSize = this.map.size();
		for(Order key : this.map.keySet()) 
			if(key.getId() == orderId) { 
				this.map.remove(key); 
				break;
			} 
		this.updateRestaurant(); 
		assert(preSize == this.map.size()+1);
	} 
	
	/**
	 * 
	 * @param !map.isEmpty() 
	 * @pre map.size() > 0
	 * @post map.size() == map@pre.size() - 1
	 */
	public void processByTable(int tableID) { 
		assert(this.map.size()>0); 
		int preSize = this.map.size();
		for(Order key : this.map.keySet()) 
			if(key.getTableId() == tableID) { 
				this.map.remove(key); 
				break;
			} 
		this.updateRestaurant(); 
		assert(preSize == this.map.size()+1);
	}
	/**
	 * 
	 * @param !map.isEmpty() 
	 * @pre !list.isEmpty()
	 * @post map.size() == map@pre.size() + 1
	 */
	public void placeOrder(int orderId,int tableId,ArrayList<MenuItem> list) { 
		assert(!this.map.isEmpty()); 
		int preSize = this.map.size();
		Order order = new Order(orderId, tableId); 
		this.map.put(order, list);  
		this.setChanged();
		this.notifyObservers();  
		this.updateRestaurant();  
		assert(preSize == this.map.size()-1);
	}

	public void initialiseRestaurant() {
		Restaurant restaurant = RestaurantSerializor.deserializeRestaurant(); 
		this.setMap(restaurant.getMap()); 
		this.setMenu(restaurant.getMenu());
	} 
	
	public void updateRestaurant() {
		RestaurantSerializor.serialize(this);
	}  
	
	public String toString() {
		String out = "The items in the menu are :\n";  
		for(int i=0;i<this.menu.size();i++) 
			out+=(i+1)+"."+this.menu.get(i).toString()+"\n"; 
		out+="The orders are :\n";  
		int index=1;
		for(Map.Entry<Order, ArrayList<MenuItem>> entry : this.map.entrySet()) {
			Order o = entry.getKey(); 
			ArrayList<MenuItem> list = entry.getValue(); 
			out +=index + "." + o.toString() + " and contains the following :\n"; 
			index++;
			for(int i=0;i<list.size();i++) 
				out+=(i+1)+"."+list.get(i).toString()+"\n"; 
		} 
		
		return out;
		
	} 
	
	public JTable getTabelMenu() { 
		Object[][] tableData = new Object[this.menu.size()][3]; 
		Object[] tableHeader = new Object[3]; 
		tableHeader[0] = (Object) new String("Id"); 
		tableHeader[1] = (Object) new String("Name"); 
		tableHeader[2] = (Object) new String("Price"); 
		for(int i=0;i<this.menu.size();i++) { 
			tableData[i][0] = (Object) i;
			tableData[i][1] = (Object) this.menu.get(i).getName(); 
			tableData[i][2] = (Object) this.menu.get(i).getPrice();
		}
		JTable table = new JTable(tableData,tableHeader); 
		return table;
	}
	
	public JTable getTableOrders() { 
		int count = 0; 
		for(Map.Entry<Order, ArrayList<MenuItem>> entry : this.map.entrySet()) {
			for(@SuppressWarnings("unused") MenuItem i : entry.getValue()) 
				count++;
		}
		Object[][] tableData = new Object[count][5]; 
		Object[] tableHeader = new Object[5]; 
		tableHeader[0] = (Object) new String("Order ID"); 
		tableHeader[1] = (Object) new String("Table ID"); 
		tableHeader[2] = (Object) new String("Date"); 
		tableHeader[3] = (Object) new String("Name"); 
		tableHeader[4] = (Object) new String("Price"); 
		int i=0;
		for(Map.Entry<Order, ArrayList<MenuItem>> entry : this.map.entrySet()) {
			Order order = entry.getKey(); 
			ArrayList<MenuItem> list = entry.getValue(); 
			tableData[i][0] = (Object)order.getId(); 
			tableData[i][1] = (Object)order.getTableId(); 
			tableData[i][2] = (Object)order.getDate(); 
			for(MenuItem item : list) {
				tableData[i][3] = (Object) item.getName(); 
				tableData[i][4] = (Object) item.getPrice(); 
				i++; 
			} 
		} 
		return new JTable(tableData,tableHeader);
	}
	
	public static void main (String[] args) {
		Restaurant r = new Restaurant(); 
		
		/*
		r.mockInitialise();
		MenuItem product = new MenuItem("Cartofi Prajiti", 5); 
		MenuItem product2 = new MenuItem("Cheeseburger", 18);  
		ArrayList<MenuItem> items = new ArrayList<MenuItem>(); 
		items.add(product); 
		items.add(product2);
		CompositeProduct product3 = new CompositeProduct("5minMenu" , items); 
		product3.computePrice();  
		r.addItemInMenu(product3); 
		ArrayList<MenuItem> list = new ArrayList<MenuItem>(); 
		list.add(product3); 
		r.placeOrder(1,1,list);  
		r.updateRestaurant(); 
		System.out.println(r.toString()); 
		*/ 
		r.initialiseRestaurant();  
		System.out.println(r.toString());
		
	}

	
	
}
