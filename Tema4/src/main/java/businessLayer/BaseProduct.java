package businessLayer;

import java.io.Serializable;

public class BaseProduct extends MenuItem implements Serializable {
	private static final long serialVersionUID = 1L;
	public BaseProduct(String name, int price) {
		super(name, price);
	}
	@Override
	public int computePrice() {
		return this.price;
	}
	
}
