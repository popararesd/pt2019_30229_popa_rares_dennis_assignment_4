package businessLayer;

import java.io.Serializable;
import java.util.ArrayList;

public class MenuItem implements Serializable { 
	private static final long serialVersionUID = 1L;
	String name; 
	int price;
	
	public MenuItem(String name , int price) {
		this.name = name; 
		this.price = price;
	} 
	
	public String getName() {
		return name;
	}

	public int getPrice() {
		return price;
	}

	public int computePrice() {
		return price;
	} 
	
	public void setPrice(int price) {
		this.price = price;
	}  
	
	public void setName(String name) {
		this.name=name;
	}
	
	public String toString() {
		String out = "Name: "+this.name+" Price: "+this.price;  
		if(this instanceof CompositeProduct) {
			ArrayList<MenuItem> list = ((CompositeProduct)this).getIngredients();  
			out+="\nContains the following :";
			for(MenuItem i : list) 
				out+="\n"+i.toString();
		}
			
		return out;
	}
}
