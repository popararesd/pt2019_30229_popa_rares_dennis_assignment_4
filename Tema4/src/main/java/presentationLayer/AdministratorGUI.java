package presentationLayer;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

import businessLayer.IRestaurantProcessing;
import businessLayer.MenuItem;
import businessLayer.Restaurant;
import dataLayer.RestaurantSerializor;

@SuppressWarnings("serial")
public class AdministratorGUI extends JPanel {
	
	private JTable menuTable; 
	private JButton addItemBtn = new JButton("Add Item"); 
	private JButton deleteItemBtn = new JButton("Delete Item"); 
	private JButton editItemBtn = new JButton("Edit Item"); 
	private JPanel operationPane = new JPanel(); 
	int selectedOperation = 2; // 0 for null , 1 for add , 2 for edit , 3 for delete 
	private IRestaurantProcessing restaurant;
	
	public AdministratorGUI (Restaurant restaurant) {  
		this.restaurant = restaurant;
		this.menuTable = this.restaurant.getTabelMenu();  
		JScrollPane scrollPane = new JScrollPane(this.menuTable); 
		this.menuTable.setFillsViewportHeight(true);
		this.setLayout(new GridLayout(1, 2)); 
		this.add(scrollPane);   
		JPanel pane = new JPanel(new GridLayout(4,1));
		JPanel auxliaryPanel = new JPanel(new FlowLayout(FlowLayout.CENTER)); 
		pane.add(new JPanel()); 
		auxliaryPanel.add(this.addItemBtn); 
		auxliaryPanel.add(this.deleteItemBtn); 
		auxliaryPanel.add(this.editItemBtn); 
		pane.add(auxliaryPanel); 
		pane.add(this.operationPane); 
		this.add(pane); 
		this.addItemBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				AdministratorGUI.this.setOperation(1); 
				AdministratorGUI.this.updatePanel();
			}
		}); 
		this.editItemBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				AdministratorGUI.this.setOperation(2); 
				AdministratorGUI.this.updatePanel();
			}
		}); 
		this.deleteItemBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				AdministratorGUI.this.setOperation(3); 
				AdministratorGUI.this.updatePanel();
			}
		});
	}  
	public void setOperation(int i) {
		this.selectedOperation = i;
	} 
	public int getOperation() {
		return this.selectedOperation; 
	}
	public void updatePanel() {
		switch(this.selectedOperation) {
		case 0 : this.operationPane = new JPanel(); 
				 break;
		case 1 : this.operationPane = new AddFrame(); 
				 break;  
		case 2 : this.operationPane = new EditFrame(); 
				 break; 
		case 3 : this.operationPane = new DeleteFrame(); 
				 break;
		default : this.operationPane = new JPanel(); 
				  break;
		} 
		this.removeAll();  
		this.menuTable = this.restaurant.getTabelMenu();  
		JScrollPane scrollPane = new JScrollPane(this.menuTable); 
		this.menuTable.setFillsViewportHeight(true);
		this.setLayout(new GridLayout(1, 2)); 
		this.add(scrollPane);   
		JPanel pane = new JPanel(new GridLayout(4,1));
		JPanel auxliaryPanel = new JPanel(new FlowLayout(FlowLayout.CENTER)); 
		pane.add(new JPanel()); 
		auxliaryPanel.add(this.addItemBtn); 
		auxliaryPanel.add(this.deleteItemBtn); 
		auxliaryPanel.add(this.editItemBtn); 
		pane.add(auxliaryPanel); 
		pane.add(this.operationPane); 
		this.add(pane);
		this.revalidate();  
		this.repaint(); 
	}
	
	public class AddFrame extends JPanel{
		private JTextField tfName = new JTextField("Name",15); 
		private JTextField tfPrice = new JTextField("Price",5); 
		private JButton addBtn = new JButton("Add"); 
		public AddFrame () {
			this.setLayout(new FlowLayout(FlowLayout.CENTER)); 
			this.add(this.tfName); 
			this.add(this.tfPrice); 
			this.add(this.addBtn); 
			this.addBtn.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent e) { 
					MenuItem menuItem = new MenuItem(AddFrame.this.tfName.getText(),Integer.parseInt(AddFrame.this.tfPrice.getText()) );
					AdministratorGUI.this.restaurant.addItemInMenu(menuItem); 
					AdministratorGUI.this.setOperation(0);
					AdministratorGUI.this.updatePanel(); 
					AddFrame.this.tfName.setText("Name"); 
					AddFrame.this.tfPrice.setText("Price");
				}
			});
		}
	}  
	
	public class EditFrame extends JPanel{ 
		private JTextField tfCurrentName = new JTextField("Current Name",15);
		private JTextField tfName = new JTextField("New Name",15); 
		private JTextField tfPrice = new JTextField("New Price" , 5); 
		private JButton editBtn = new JButton("Edit"); 
		public EditFrame() {
			 this.setLayout(new FlowLayout(FlowLayout.CENTER)); 
			 this.add(tfCurrentName);
			 this.add(this.tfName); 
			 this.add(this.tfPrice); 
			 this.add(this.editBtn); 
			 this.editBtn.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent e) { 
					String CurrentName = EditFrame.this.tfCurrentName.getText(); 
					String NewName = EditFrame.this.tfName.getText(); 
					int price = Integer.parseInt(EditFrame.this.tfPrice.getText());
					AdministratorGUI.this.restaurant.editMenuItem(CurrentName, NewName, price); 
					AdministratorGUI.this.setOperation(0);
					AdministratorGUI.this.updatePanel(); 
					EditFrame.this.tfCurrentName.setText("Current Name"); 
					EditFrame.this.tfName.setText("New Name"); 
					EditFrame.this.tfPrice.setText("New Price");
				}
			});
		}
	} 
	
	public class DeleteFrame extends JPanel{
		private JTextField tfName = new JTextField("Name",15); 
		private JButton deleteBtn = new JButton("Delete"); 
		public DeleteFrame() {
			 this.setLayout(new FlowLayout(FlowLayout.CENTER)); 
			 this.add(this.tfName); 
			 this.add(this.deleteBtn); 
			 this.deleteBtn.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent e) {
					String name = DeleteFrame.this.tfName.getText(); 
					AdministratorGUI.this.restaurant.removeItemFromMenu(name); 
					AdministratorGUI.this.setOperation(0);
					AdministratorGUI.this.updatePanel(); 
					DeleteFrame.this.tfName.setText("Name");
				}
			});
		}
	}
	
	public static void main(String[] args) { 
		Restaurant rest = new Restaurant(); 
		rest = RestaurantSerializor.deserializeRestaurant();
		JFrame frame = new JFrame();  
		frame.setSize(500, 600);  
		AdministratorGUI g = new AdministratorGUI(rest);
		frame.add(g);
		frame.setVisible(true);  
		g.updatePanel(); 
		g.setOperation(3); 
		g.updatePanel();
	}
}
