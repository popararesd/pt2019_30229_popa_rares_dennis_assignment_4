package presentationLayer;
import java.awt.Color;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import businessLayer.Restaurant;

@SuppressWarnings("serial")
public class View extends JFrame {
	
	private AdministratorGUI administrator; 
	private WaiterGUI waiter; 
	private ChefGUI chef; 
	private FlashingTab tabs = new FlashingTab();
	private boolean flashing = false; 
	private Restaurant restaurant;
	
	@SuppressWarnings("deprecation")
	public View(Restaurant restaurant) {  
		this.setTitle("Restaurant Application");
		this.setSize(500,600);  
		this.restaurant = restaurant; 
		this.administrator = new AdministratorGUI(this.restaurant); 
		this.waiter = new WaiterGUI(this.restaurant); 
		this.chef = new ChefGUI(); 
		this.restaurant.addObserver(chef);
		tabs.addTab("Administrator",this.administrator);
		tabs.addTab("Waiter", this.waiter); 
		tabs.addTab("Chef", this.chef);
		this.add(tabs); 
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE); 
		this.tabs.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				// TODO Auto-generated method stub  
				if(View.this.tabs.getSelectedIndex() == 2 && View.this.flashing == true) {
					View.this.stopFlashing(); 
					View.this.flashing = false; 
				}  
			}
		});
		this.chef.addComponentListener(new ComponentListener() {
			public void componentShown(ComponentEvent e) {  
				
			}

			public void componentResized(ComponentEvent e) {
				// TODO Auto-generated method stub 
				View.this.flashTab(); 
				View.this.flashing = true;
				
			}

			public void componentMoved(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}

			public void componentHidden(ComponentEvent e) {
			}
		}); 
	} 
	public void flashTab() {
		this.tabs.flash(2, Color.red, Color.yellow);
	} 
	public void stopFlashing() {
		this.tabs.clearFlashing();
	} 
	public void update() {
		this.chef.setVisible(true);
	} 
	public ChefGUI getChef() {
		return this.chef;
	}
}
