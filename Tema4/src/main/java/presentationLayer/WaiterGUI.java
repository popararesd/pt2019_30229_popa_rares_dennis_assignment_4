package presentationLayer;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.*;

import businessLayer.IRestaurantProcessing;
import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;
import dataLayer.FileWriter;
import dataLayer.RestaurantSerializor;

@SuppressWarnings("serial")
public class WaiterGUI extends JPanel {

	private JTable orderTable;  
	private JTable menuTable;
	private JButton addOrderBtn = new JButton("Add Order"); 
	private JButton computeOrderBtn = new JButton("Compute Order");  
	private JButton generateBill = new JButton("Generate Bill");
	private JPanel operationPane = new JPanel(); 
	int selectedOperation = 2;  
	private IRestaurantProcessing restaurant;
	public WaiterGUI(Restaurant restaurant) 
	{  
		this.restaurant = restaurant;
		this.orderTable = this.restaurant.getTableOrders(); 
		this.menuTable = this.restaurant.getTabelMenu(); 
		JPanel auxPane = new JPanel(new GridLayout(2,1)); 
		JScrollPane scroll1 = new JScrollPane(this.menuTable); 
		this.menuTable.setFillsViewportHeight(true);
		JScrollPane scroll2 = new JScrollPane(this.orderTable); 
		this.orderTable.setFillsViewportHeight(true);
		auxPane.add(scroll1); 
		auxPane.add(scroll2);
		this.setLayout(new GridLayout(1, 2)); 
		this.add(auxPane);   
		JPanel pane = new JPanel(new GridLayout(4,1));
		JPanel auxliaryPanel = new JPanel(new FlowLayout(FlowLayout.CENTER)); 
		pane.add(new JPanel()); 
		auxliaryPanel.add(this.addOrderBtn); 
		auxliaryPanel.add(this.computeOrderBtn); 
		auxliaryPanel.add(this.generateBill); 
		pane.add(auxliaryPanel); 
		pane.add(this.operationPane); 
		this.add(pane); 
		this.addOrderBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				WaiterGUI.this.setOperation(1); 
				WaiterGUI.this.updatePanel();
			}
		}); 
		this.computeOrderBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				WaiterGUI.this.setOperation(2); 
				WaiterGUI.this.updatePanel();
			}
		}); 
		this.generateBill.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				WaiterGUI.this.setOperation(3); 
				WaiterGUI.this.updatePanel();
			}
		});
	}  
	public void setOperation(int i) {
		this.selectedOperation = i;
	} 
	public int getOperation() {
		return this.selectedOperation; 
	}
	public void updatePanel() {
		switch(this.selectedOperation) {
		case 0 : this.operationPane = new JPanel(); 
				 break;
		case 1 : this.operationPane = new AddFrame(); 
				 break;  
		case 2 : this.operationPane = new ComputeFrame(); 
				 break; 
		case 3 : this.operationPane = new BillFrame(); 
				 break;
		default : this.operationPane = new JPanel(); 
				  break;
		} 
		this.removeAll();
		this.orderTable = this.restaurant.getTableOrders(); 
		this.menuTable = this.restaurant.getTabelMenu(); 
		JPanel auxPane = new JPanel(new GridLayout(2,1)); 
		JScrollPane scroll1 = new JScrollPane(this.menuTable); 
		this.menuTable.setFillsViewportHeight(true);
		JScrollPane scroll2 = new JScrollPane(this.orderTable); 
		this.orderTable.setFillsViewportHeight(true);
		auxPane.add(scroll1); 
		auxPane.add(scroll2);
		this.setLayout(new GridLayout(1, 2)); 
		this.add(auxPane);    
		JPanel pane = new JPanel(new GridLayout(4,1));
		JPanel auxliaryPanel = new JPanel(new FlowLayout(FlowLayout.CENTER)); 
		pane.add(new JPanel()); 
		auxliaryPanel.add(this.addOrderBtn); 
		auxliaryPanel.add(this.computeOrderBtn); 
		auxliaryPanel.add(this.generateBill); 
		pane.add(auxliaryPanel); 
		pane.add(this.operationPane); 
		this.add(pane);
		this.revalidate();  
		this.repaint(); 
	}
	
	public class AddFrame extends JPanel{ 
		private JTextField tfId = new JTextField("Order Id",5); 
		private JTextField tfTable = new JTextField("Table Id",5);
		private JTextField tfItems = new JTextField("Items separated by commas",20);  
		private JButton addBtn = new JButton("Add"); 
		public AddFrame () {
			this.setLayout(new FlowLayout(FlowLayout.CENTER)); 
			this.add(this.tfId); 
			this.add(this.tfTable);  
			this.add(this.tfItems);
			this.add(this.addBtn); 
			this.addBtn.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent e) { 
					int orderId = Integer.parseInt(AddFrame.this.tfId.getText()); 
					int tableId = Integer.parseInt(AddFrame.this.tfTable.getText());  
					String items = AddFrame.this.tfItems.getText(); 
					String[] itemList = items.split("\\s*,\\s*"); 
					ArrayList<MenuItem> menu = new ArrayList<MenuItem>(WaiterGUI.this.restaurant.getMenu()); 
					ArrayList<MenuItem> list = new ArrayList<MenuItem>();
					for(int i=0;i<itemList.length;i++) {
						int index = Integer.parseInt(itemList[i]); 
						list.add(menu.get(index));
					}
					
					WaiterGUI.this.restaurant.placeOrder(orderId, tableId, list); 
					AddFrame.this.tfId.setText("Order Id"); 
					AddFrame.this.tfTable.setText("TableId"); 
					AddFrame.this.tfItems.setText("Items separated by commas");  
					WaiterGUI.this.setOperation(0);
					WaiterGUI.this.updatePanel();
				}
			});
		}
	} 
	public class ComputeFrame extends JPanel{
		private JTextField tfId = new JTextField("Order Id"); 
		private JTextField tfResult = new JTextField("Result");
		private JButton showBtn = new JButton("Compute"); 
		public ComputeFrame () { 
			this.tfResult.setEditable(false);
			this.setLayout(new FlowLayout(FlowLayout.CENTER)); 
			this.add(this.tfId);  
			this.add(this.tfResult);
			this.add(this.showBtn);   
			this.showBtn.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent e) {
					ComputeFrame.this.tfResult.setText(String.valueOf(WaiterGUI.this.restaurant.getTotalPrice(Integer.parseInt(ComputeFrame.this.tfId.getText()))));
				}
			});
		}
	} 
	public class BillFrame extends JPanel{
		private JTextField tfId = new JTextField("Order ID",5); 
		private JButton billBtn = new JButton("Generate Bill"); 
		public BillFrame() {
			this.setLayout(new FlowLayout(FlowLayout.CENTER)); 
			this.add(this.tfId); 
			this.add(this.billBtn);  
			this.billBtn.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent e) {
					int orderId = Integer.parseInt(BillFrame.this.tfId.getText()); 
					//Map.Entry<Order, ArrayList<MenuItem>> entry = null; 
					for(Map.Entry<Order, ArrayList<MenuItem>> entry : WaiterGUI.this.restaurant.getMap().entrySet()) {
						if(entry.getKey().getId() == orderId) {
							FileWriter.createBill(entry); 
							break;
						}
					} 
					WaiterGUI.this.restaurant.processByOrder(orderId);
					BillFrame.this.tfId.setText("Order ID"); 
					WaiterGUI.this.setOperation(0); 
					WaiterGUI.this.updatePanel();
				}
			});
		}
	}
	public static void main(String[] args) { 
		Restaurant rest = new Restaurant(); 
		rest = RestaurantSerializor.deserializeRestaurant();
		JFrame frame = new JFrame(); 
		frame.setSize(500,600); 
		WaiterGUI w = new WaiterGUI(rest); 
		frame.add(w); 
		frame.setVisible(true);  
		w.setOperation(3);
		w.updatePanel();
		
	}
}
