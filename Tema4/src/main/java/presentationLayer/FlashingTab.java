package presentationLayer;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.Timer;

@SuppressWarnings("serial")
public class FlashingTab extends JTabbedPane {
	
	JButton btn = new JButton("Stop"); 
	
	public void init() {
	btn.addActionListener(new ActionListener() {
		
		public void actionPerformed(ActionEvent e) {
			
			FlashingTab.this.clearFlashing();
			
		}
	});  
	}
	
	
	
	private int _tabIndex;
	private Color _background;
	private Color _foreground;
	private Color _savedBackground;
	private Color _savedForeground;
	private Timer timer = new Timer(1000, new ActionListener() {
		
		private boolean on = false;
		
		public void actionPerformed(ActionEvent e) {
			flash(on);
			on = !on;
		}
	});

	public void flash(int tabIndex, Color foreground, Color background) {

		_tabIndex = tabIndex;
		_savedForeground = getForeground();
		_savedBackground = getBackground();
		_foreground = foreground;
		_background = background;
		timer.start();
	}

	private void flash(boolean on) {
		if (on) {
			if (_foreground != null) {
				setForegroundAt(_tabIndex, _foreground);
			}
			if (_background != null) {
				setBackgroundAt(_tabIndex, _background);
			}
		} else {
			if (_savedForeground != null) {
				setForegroundAt(_tabIndex, _savedForeground);
			}
			if (_savedBackground != null) {
				setBackgroundAt(_tabIndex, _savedBackground);
			}
		}
		repaint();
	}

	public void clearFlashing() {
		timer.stop();
		setForegroundAt(_tabIndex, _savedForeground);
		setBackgroundAt(_tabIndex, _savedBackground); 
		repaint();
	}
	
	public JButton getBtn () {
		return this.btn;
	}
	
	public static void main(String[] args) {
		JFrame frame = new JFrame();
		FlashingTab tabs = new FlashingTab(); 
		tabs.init();
		tabs.addTab("ABC", tabs.getBtn());
		tabs.addTab("XYZ", new JLabel("Tab 2"));
		tabs.flash(1, Color.red, Color.yellow); 
		frame.add(tabs);
		frame.pack();
		frame.setVisible(true); 
		//tabs.clearFlashing();
	}
}