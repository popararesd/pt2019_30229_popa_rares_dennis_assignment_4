package presentationLayer;
import java.awt.Dimension;
import java.util.*;

import javax.swing.*;

import businessLayer.Restaurant;
import dataLayer.RestaurantSerializor;

@SuppressWarnings({ "serial", "deprecation" })
public class ChefGUI extends JPanel implements Observer {

	private JTable orderTable;
	//private Restaurant restaurant;
	
	public ChefGUI() { 
		Restaurant restaurant = new Restaurant(); 
		restaurant = RestaurantSerializor.deserializeRestaurant();
		this.orderTable = restaurant.getTableOrders(); 
		JScrollPane pane = new JScrollPane(this.orderTable);
		this.orderTable.setFillsViewportHeight(true);
		this.add(pane);

	}
	public void update(Observable o, Object arg) {   
		Dimension d= new Dimension(); 
		d.setSize(this.getSize().getWidth()-1.1,this.getSize().getHeight()-1.1);
		this.resize(d);
	}
}
