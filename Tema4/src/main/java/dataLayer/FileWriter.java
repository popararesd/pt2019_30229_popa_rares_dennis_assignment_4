package dataLayer;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Map;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import businessLayer.*;

public class FileWriter { 
	private static final String billPattern = new String("bill__"); 
	/*
	Document document = new Document();
	try {
	
	PdfWriter.getInstance(document, new FileOutputStream("BillNo"+BillWindow.this.tfId.getText()+".pdf"));
	 
	document.open();
	Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK); 
	Font title = FontFactory.getFont(FontFactory.TIMES_BOLDITALIC,30, BaseColor.BLACK);
	Image img = Image.getInstance("C:\\Users\\Rares\\Desktop\\angrysushi.jpg"); 
	document.add(img);  
	String[] s = (new OrderDao()).getOrderDetails(Integer.parseInt(BillWindow.this.tfId.getText()));
	String[] headers = {"Name :" , "Adress :" , "Email :" , "Product name :" , "Price : " , "Balance :" , "Amount :"};
	int index = 0; 
	Chunk c = new Chunk("\n\n\n\n\n\n\n");
	document.add(c); 
	Chunk q = new Chunk("INVOICE",title); 
	Paragraph par = new Paragraph(q); 
	par.setAlignment(Element.ALIGN_CENTER);
	document.add(par);
	for(String x : s) { 
	Chunk chunk = new Chunk(headers[index]+x+"\n", font);  
	Paragraph p = new Paragraph(chunk); 
	p.setAlignment(Element.ALIGN_CENTER);
	document.add(p); 
	index++;
	}
	} 
	catch(Exception ex) {
		
	} 
	finally {
	document.close(); 
	}
	*/
	public static void createBill(Map.Entry<Order, ArrayList<MenuItem>> entry) { 
		StringBuilder billName = new StringBuilder(billPattern); 
		Order order = entry.getKey();  
		Restaurant restaurant = new Restaurant(); 
		restaurant = RestaurantSerializor.deserializeRestaurant();
		int totalPrice = restaurant.getTotalPrice(order.getId());
		billName.insert(5,order.getId()); 
		billName.insert(7,order.getTableId());  
		Document document = new Document(); 
		try { 
			PdfWriter.getInstance(document, new FileOutputStream(billName+".pdf"));
			document.open();
			Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK); 
			Font title = FontFactory.getFont(FontFactory.TIMES_BOLDITALIC,30, BaseColor.BLACK); 
			String[] data = new String[entry.getValue().size()+2];  
			data[0] = "Items ordered and the prices";
			int j=1;
			for(MenuItem i : entry.getValue()) {
				data[j] = i.getName() + " " + i.getPrice(); 
				j++;
			}  
			data[entry.getValue().size()+1] = "Total :" + totalPrice; 
			Chunk c = new Chunk("\n\n\n\n\n\n\n");
			document.add(c); 
			Chunk q = new Chunk("BILL",title); 
			Paragraph par = new Paragraph(q); 
			par.setAlignment(Element.ALIGN_CENTER);
			document.add(par); 
			for(String x : data) { 
				Chunk chunk = new Chunk(x+"\n", font);  
				Paragraph p = new Paragraph(chunk); 
				p.setAlignment(Element.ALIGN_CENTER);
				document.add(p); 
			}
		} 
		catch(Exception ex) {
			System.out.println(ex.getMessage());
		} 
		finally {
			document.close();
		}
	}
}
