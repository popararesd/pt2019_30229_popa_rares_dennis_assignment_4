package dataLayer;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import businessLayer.Restaurant;

public class RestaurantSerializor implements Serializable { 
	
	private static final long serialVersionUID = 1L;
	private static final String fileName = "restaurant.ser";
	
	public static void serialize(Restaurant restaurant) {
		try {
			FileOutputStream outputFile = new FileOutputStream(fileName); 
			ObjectOutputStream out = new ObjectOutputStream(outputFile); 
			out.writeObject(restaurant); 
			out.close();  
			outputFile.close();
		} 
		catch(IOException ex) {
			System.out.println(ex.getMessage());
		}
	} 
	
	
	public static Restaurant deserializeRestaurant() {
		Restaurant restaurant = null; 
		try {
			FileInputStream inputFile = new FileInputStream(fileName); 
			ObjectInputStream in = new ObjectInputStream(inputFile);  
			restaurant = (Restaurant) in.readObject();  
			in.close();  
			inputFile.close();
		}  
		catch(Exception ex) {
			System.out.println(ex.getMessage());
		} 
		return restaurant;
	}
}
