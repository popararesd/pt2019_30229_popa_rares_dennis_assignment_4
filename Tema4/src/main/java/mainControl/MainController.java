package mainControl;

import businessLayer.Restaurant;
import dataLayer.RestaurantSerializor;
import presentationLayer.View;

public class MainController {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Restaurant restaurant = new Restaurant(); 
		restaurant = RestaurantSerializor.deserializeRestaurant(); 
		View v = new View(restaurant);
	}

}
